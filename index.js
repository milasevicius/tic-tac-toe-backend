import express from 'express'
import cors from 'cors'

const app = express()
app.use(express.json())
app.use(cors())

const db = []

app.get('/', function (req, res) {
  res.json(db)
})

app.post('/', (req, res) => {
  db.push(req.body)

  res.sendStatus(201)
})

app.listen(3001)
