How to start project?

* `docker build -t tic-tac-toe-backend .`
* `docker run -it -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules -p 3001:3001 --rm tic-tac-toe-backend`
